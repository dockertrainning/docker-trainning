https://github.com/docker/docker.github.io/blob/master/storage/volumes.md


#Comandos volúmenes
docker volume create my-vol
docker volume ls
docker volume inspect my-vol
docker volume rm my-vol


# Mount
docker run -d \
  --name devtest \
  --mount source=myvol2,target=/app \
  nginx:latest

# Volume
  docker run -d \
  --name devtest \
  -v myvol2:/app \
  nginx:latest

# Readonly
  docker run -d \
  --name=nginxtest \
  -v nginx-vol:/usr/share/nginx/html:ro \
  nginx:latest