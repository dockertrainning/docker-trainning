#Obtiene la última versión de la imagen solicitada
docker pull nginx

#Ejecuta 
docker run -d -p 10000:80 --name nginx nginx 

#Lista los procesos activos
docker ls


#Inspecciona el container nginx
docker inspect nginx

#Muestra los logs del container dado
docker logs nginx

#Detiene el container dado
docker stop nginx

#Elimina el container dado
docker rm nginx



